var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
    , apiRoute = require('./routes/apiRoute')
    , webRoute = require('./routes/webRoute');

var mongoose = require('mongoose'),
    mongoConfig = require('./config/mongo');
var session = require('express-session')


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')

app.use(session({
  secret: 'nodepad app shahab',
  resave: false,
  saveUninitialized: true
}));

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Initializing Mongoose
mongoose.connect( mongoConfig.mongoURI,function(err){
  if (err){
      console.log('Error while connecting to mongodb: ' + err);
      console.log('Start your local mongo service or provide a valid mongoURL in the environment variable: MONGO_URL');
      console.log('Exiting Process');
      process.exit(1);
  }
  else {
    console.log('Connected To Mongo Database with URL: ' + mongoConfig.mongoURI);
  }
});

// Routes
app.use('/api', apiRoute);
app.get('/:page?/:id?', webRoute.index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
