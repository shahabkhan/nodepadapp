var mongoURI = '';
if (process.env.MONGO_URL){
  mongoURI = process.env.MONGO_URL;
}
else {
  mongoURI = 'mongodb://localhost/nodePad';
}

exports.mongoURI = mongoURI;
