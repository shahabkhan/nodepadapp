var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Note = new Schema({
  name: {type: String, required:true},
  data: {type: String, required:true},
  createdAt: {type: Date, default: Date.now()}
});

var UserDetail = new Schema({
  name:String,
  email:{type: String, unique:true, required:true},
  password:{type: String, required:true},
  notes:[Note],
  createdAt: {type: Date, default: Date.now()}
});

module.exports = mongoose.model('users', UserDetail);

