var User = require('../models/User');

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

exports.register = function (userInfo, cb) {
  if (IsEmail(userInfo.email) && userInfo.password && userInfo.name && userInfo.password != '' && userInfo.name != '') {
    // Check if user exists
    User.count({email: userInfo.email}, function (err, count) {
      if (err)
        cb('DB Error:' + err);
      else {
        if (count == 0) {
          new User(userInfo).save(function (err, saved) {
            cb(err, saved);
          })
        }
        else
          cb('Email Address Already Exists', null);
      }
    });

  }
  else {
    cb('Invalid Email', null)
  }
};

exports.login = function (email, password, cb) {
  if (IsEmail(email)) {
    User.findOne({email: email}, {__v: 0}, function (err, data) {
      if (err)
        cb('DB Error:' + err);
      else {
        if (data == null)
          cb('Email Address Not Found', null);
        else {
          data = data.toObject();
          if (data.password === password) {
            delete data.password;
            // Do not send password with user details
            cb(null, data)
          } else {
            cb('Invalid password', null);
          }
        }
      }
    })

  } else {
    cb('Invalid Email', null)
  }
};

exports.addNote = function (userId, noteInfo, cb) {
  if (noteInfo.name != '' && noteInfo.data != '') {
    noteInfo.createdAt = new Date();
    console.log('adding note', userId, noteInfo);
    User.update({_id: userId}, {$push: {'notes': noteInfo}}, {upsert: true}, function (err, update) {
      cb(err, update)

    })
  } else {
    cb('Invalid Form Details', null)
  }
};

exports.editNote = function (userId, noteInfo, cb) {
  if (noteInfo.id != '' && noteInfo.name != '' && noteInfo.data != '') {
    console.log('editing note', userId, noteInfo);
    var updateQuery = {$set:{'notes.$.data':noteInfo.data, 'notes.$.name':noteInfo.name}};
    User.update({_id:userId,'notes._id':noteInfo.id},updateQuery,{upsert:true},function(err,update){
      cb(err,update)
    });

  } else {
    cb('Invalid Form Details', null)
  }
};

exports.deleteNote = function (userId, noteId, cb) {
  if (noteId != '') {
    User.update({_id: userId}, {$pull: {'notes': {_id: noteId}}}, {upsert: true}, function (err, update) {
      cb(err, update)
    })
  } else {
    cb('Invalid Note Id Details', null)
  }
}

exports.getNotes = function (userId, noteId, cb) {
  if (noteId) {
    User.findOne({_id: userId}, {'notes': 1}, function (err, data) {
      if (!err && data && data.notes.length > 0) {
        var requiredNote = null;
        data.notes.forEach(function (val) {
          if (val._id == noteId) {
            requiredNote = val;
          }
        });
        cb(null, [requiredNote])

      } else {
        cb(err, null)
      }
    })

  } else {
    User.findOne({_id: userId}, {'notes._id': 1, 'notes.name': 1, 'notes.createdAt': 1}, function (err, data) {
      if (!err && data.notes.length > 0) {
        cb(err, data.notes && data.notes.reverse())
      } else {
        cb(err, null)
      }
    })

  }
};
