'use strict';
var express = require('express');
var router = express.Router();
var UserController = require('../controllers/UserController');

router.post('/register', function (req, res) {
  var userInfo = {
    name: req.body.name || null,
    email: req.body.email || null,
    password: req.body.password || null
  };
  UserController.register(userInfo, function (err, response) {
    if (err) {
      res.status(401).json({error: err})
    }
    else {
      res.status(200).json({error: null, message: "User Created Successfully"})
    }

  })

});

router.post('/login', function (req, res) {
  var userInfo = {
    email: req.body.email || null,
    password: req.body.password || null
  };
  UserController.login(userInfo.email,userInfo.password, function (err, response) {
    if (err) {
      res.status(401).json({error: err})
    }
    else {
      req.session.currentUser = response;
      res.status(200).json({error: null, user: response})
    }
  })
});

router.post('/addNote', function (req, res) {
  if (req.session.currentUser){
    var noteInfo = {
      name: req.body.name || null,
      data: req.body.data || null
    };
    UserController.addNote(req.session.currentUser._id,noteInfo, function (err, response) {
      if (err) {
        res.status(401).json({error: err})
      }
      else {
        res.status(200).json({error: null, data: 'Added Successfully'})
      }
    })
  }else {
    res.status(401).send({error:'UnAuthorised'})
  }

});

router.post('/editNote', function (req, res) {
    if (req.session.currentUser){
        var noteInfo = {
            id:req.body.id || null,
            name: req.body.name || null,
            data: req.body.data || null
        };
        UserController.editNote(req.session.currentUser._id,noteInfo, function (err, response) {
            if (err) {
                res.status(401).json({error: err})
            }
            else {
                res.status(200).json({error: null, data: 'Note Edited Successfully'})
            }
        })
    }else {
        res.status(401).send({error:'UnAuthorised'})
    }

});

router.get('/deleteNote/:noteId',function(req,res){
  if (req.session.currentUser && req.params.noteId){
    UserController.deleteNote(req.session.currentUser._id, req.params.noteId, function(err,resp){
      console.log('on server',err,resp);
      if (err){
        res.status(401).send({error:err})
      }else {
        res.status(200).send({error:null,data:resp})
      }
    });

  }
  else{
      res.status(401).send({error:'UnAuthorized'})
  }

  });

router.get('/getCurrentUser',function(req,res){
  if (req.session.currentUser){
    res.status(200).send({error:null,user:req.session.currentUser})
  }else {
    res.status(401).send({error:'No User Found',user:null})
  }
});

router.get('/getNotesForUser/:noteId?',function(req,res){
  if (req.session.currentUser){
    UserController.getNotes(req.session.currentUser._id,req.params.noteId,function(err,notes){
        console.log('notes',err,notes)
      if (err){
        res.status(401).send({error:err,notes:null})
      }else {
        res.status(200).send({error:null,notes:notes})
      }

    })
  }else {
    res.status(401).send({error:'No User Found',notes:null})
  }
});

router.get('/logout',function(req,res){
  console.log('session',req.session.currentUser)
  if (req.session.currentUser){
    req.session.currentUser = null;
  }
  console.log('deleted session',req.session.currentUser)
  res.status(200).send('logged out');
});

module.exports = router;