angular.module('NodePadApp').factory( 'AuthService', function($http,$q) {
  var currentUser = null;

  return {
    login: function(email,password,cb) {
      var payload = {
        email:email,
        password:password
      };
          $http.post('/api/login',payload).
              success(function (data) {
            currentUser = data.user;
            return cb();
          }).
              error(function (err) {
                return cb(err);
              });
    },
    logout: function() {
      currentUser = null;
      console.log('trying to logout')
      $http.get('/api/logout').
          success(function(data,status){
            console.log('logged out',data,status)
          }).error(function (err) {
            console.log('Unable to logout')
          });
    },
    getCurrentUserFromServer: function(cb){
      var that = this;
      $http.get('/api/getCurrentUser').
          success(function(data,status){
            if (status == 200){
              currentUser = data.user;
              cb(null)
            }else {
              currentUser = null;
              cb('No User Found')
            }

          }).error(function (err) {
            currentUser = null;
            that.logout();
            cb (err)

          });
    },
    currentUser: function() {
      var deferred = $q.defer();
      if (!currentUser){
        this.getCurrentUserFromServer(function(err){
          console.log('returning server user')
          deferred.resolve({error:err,user:currentUser});
        });
      }else {
        console.log('returning already existng user');
        deferred.resolve({error:null,user:currentUser});
      }
      return deferred.promise;
    }
  };
});