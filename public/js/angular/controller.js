//Defining Controllers Now..

var showErrorMessage = function ($scope,$timeout,msg) {
  $scope.errorOccured = true;
  $scope.errorMessage = msg;
  $timeout(function () {
    $scope.errorOccured = false;
    $scope.errorMessage = null;
  }, 2000);
};

var showSuccessMessage = function ($scope,$timeout,msg) {
  $scope.successAlert = true;
  $scope.errorOccured = false;
  $scope.successMessage = msg;
  $timeout(function () {
    $scope.successAlert = false;
    $scope.successMessage = null;
  }, 2000);
};

var toggleButtonStatus = function (val) {
  if (val == 'hide') {
    $("#submitButton").attr({value: 'Request In Process', disabled: 'disabled'});
    $("#submitButton").addClass('active');
  } else if (val == 'show') {
    $("#submitButton").attr({value: 'Submit', disabled: null});
    $("#submitButton").removeClass('active');
  }
};

var getUpdatesNotesFromServer = function(noteId,$scope,$http,$timeout){
  if (noteId){
    var responsePromise = $http.get('/api/getNotesForUser/'+noteId)

  }else {
    var responsePromise = $http.get('/api/getNotesForUser');

  }
  responsePromise.success(function (data, status, headers, config) {
    console.log('response',data,status);
    toggleButtonStatus('show');
    if (status == 200) {
      $scope.notesArray = data.notes;
    }
  });
  responsePromise.error(function (data, status, headers, config) {
    console.log('response in error',data,status);
    if (status == 401) {
      showErrorMessage($scope,$timeout,data.error);
      toggleButtonStatus('show');
    }else {
      showErrorMessage($scope,$timeout,"Error On Server");
    }
  });
}

function loginController($scope,$http,$state,$timeout,AuthService){
  $scope.email = $scope.password = '';
  $scope.submitLoginForm = function(){
    toggleButtonStatus('hide');
    if ($scope.email != '' && $scope.password!=''){
      AuthService.login($scope.email,$scope.password,function(resp){
        if (resp && resp.error){
            toggleButtonStatus('show');
            showErrorMessage($scope,$timeout,resp.error);
        }
        else {
          console.log('goto dashboard')
          $state.go('dashboard')
        }
      })

    }else {
      showErrorMessage($scope,$timeout,"Invalid Form Details");
      toggleButtonStatus('show');
    }

  }
}

function registerController($scope,$http,$timeout){
  $scope.ajaxLoading = false;
  $scope.name = $scope.email = $scope.password = '';
  $scope.submitRegistrationForm = function () {
    toggleButtonStatus('hide');
    if ($scope.name != '' && $scope.email != '' && $scope.password!=''){
      var payload = {
        name:$scope.name,
        email:$scope.email,
        password:$scope.password
      }

      var responsePromise = $http.post('/api/register',payload);
      responsePromise.success(function (data, status, headers, config) {
        console.log('response',data,status);
        toggleButtonStatus('show');
        if (status == 200) {
          $scope.name = $scope.email = $scope.password = '';
          showSuccessMessage($scope,$timeout,data.message);
        }
      });
      responsePromise.error(function (data, status, headers, config) {
        console.log('response in error',data,status);
        if (status == 401) {
          showErrorMessage($scope,$timeout,data.error);
          toggleButtonStatus('show');
        }else {
          showErrorMessage($scope,$timeout,"Error On Server");
        }
      });

    }else {
      showErrorMessage($scope,$timeout,"Invalid Form Details");
      toggleButtonStatus('show');
    }
  };


}

function dashboardController ($scope,AuthService,$state){
  $scope.currentUser = null;
  $scope.currentUserId = null;
    AuthService.currentUser().then(function(response){
        console.log('then',response);
        if (response && response.error){
            $state.go("login");
        }else if (response && response.error == null){
            $scope.currentUser = response.user;
        }else {
            $state.go("login");
        }
    });
}

function logoutController ($state,AuthService){
  console.log('logout controller');
  AuthService.logout();
  $state.go('homePage');
}

function NoteController ($scope,AuthService,$state,$timeout,$http,$stateParams){
  $scope.currentUser = null;
  $scope.currentUserId = null;
  $scope.noteName = $scope.noteContent='';
  $scope.notesArray = [];
    $scope.title = "Add";
    AuthService.currentUser().then(function(response){
    console.log('then',response);
    if (response && response.error){
      $state.go("login");
    }else if (response && response.error == null){
      $scope.currentUser = response.user;
      }else {
      $state.go("login");
    }
  });

  //get updated notes from server
  if ($state.current.name == 'viewNotes'){
    getUpdatesNotesFromServer(null,$scope,$http,$timeout);
  }

  if ($state.current.name == 'editNote'){
    getUpdatesNotesFromServer($stateParams.noteId,$scope,$http,$timeout);
    $scope.noteName = $scope.notesArray[0].name;
    $scope.noteContent = $scope.notesArray[0].data;
  }

  $scope.submitAddEditNoteForm = function(){
    if ($scope.noteName !='' && $scope.noteContent!=''){
      console.log($scope.noteName, $scope.noteContent);
      var payload = {
        name:$scope.noteName,
        data:$scope.noteContent
      }
      var responsePromise = $http.post('/api/addNote',payload);
      responsePromise.success(function (data, status, headers, config) {
        console.log('response',data,status)
        toggleButtonStatus('show');
        if (status == 200) {
          showSuccessMessage($scope,$timeout,data.data);
          $scope.noteName = $scope.noteContent='';
        }
      });
      responsePromise.error(function (data, status, headers, config) {
        console.log('response in error',data,status);
        showErrorMessage($scope,$timeout,data);
        toggleButtonStatus('show');
      });
    }else {
      showErrorMessage($scope,$timeout,"Invalid Form Details");
      toggleButtonStatus('show');
    }

  }

  $scope.deleteNote = function(index,noteId){
    if (noteId && noteId!=''){
      console.log('deleting',noteId);
      var responsePromise = $http.get('/api/deleteNote/'+noteId);
      responsePromise.success(function (data, status, headers, config) {
        console.log('response',data,status);
        toggleButtonStatus('show');
        if (status == 200) {
          $scope.notesArray.splice(index,1);
          showSuccessMessage($scope,$timeout,'Successfully Deleted');
        }
      });
      responsePromise.error(function (data, status, headers, config) {
        console.log('response in error',data,status);
        if (status == 401) {
          showErrorMessage($scope,$timeout,data.error);
          toggleButtonStatus('show');
        }else {
          showErrorMessage($scope,$timeout,"Error On Server");
        }
      });
    }
  }

  $scope.editNote = function(noteId){
    if (noteId && noteId!=''){
      console.log('editing',noteId)
      $state.go('editNote',{noteId:noteId})
    }
  }

}

function editNoteController ($scope,AuthService,$state,$timeout,$http,$stateParams){
  $scope.currentUserId = null;
  $scope.currentUser = null;
  $scope.noteName = null;
  $scope.noteContent = null;
    $scope.nonEditedNote = {};
    $scope.title = "View/Edit";
    $scope.noteEditedSuccessfully = false;

  AuthService.currentUser().then(function(response){
    console.log('then',response);
    if (response && response.error){
      $state.go("login");
    }else if (response && response.error == null){
      $scope.currentUser = response.user;
    }else {
      $state.go("login");
    }
  });

  if ($stateParams.noteId){
    var responsePromise = $http.get('/api/getNotesForUser/'+$stateParams.noteId);
    responsePromise.success(function (data, status, headers, config) {
      console.log('response',data,status);
      toggleButtonStatus('show');
      if (status == 200) {
          if (data.notes && data.notes[0]){
              $scope.nonEditedNote.noteName = $scope.noteName = data.notes[0].name;
              $scope.nonEditedNote.noteContent = $scope.noteContent = data.notes[0].data;
          }else {
              $state.go('viewNotes');
          }
      }else {
          $state.go('viewNotes');
      }
    });
    responsePromise.error(function (data, status, headers, config) {
      console.log('response in error',data,status);
      if (status == 401) {
        showErrorMessage($scope,$timeout,data.error);
        toggleButtonStatus('show');
      }else {
        showErrorMessage($scope,$timeout,"Error On Server");
      }
    });
  }else {
    $state.go('viewNotes');
  }

    $scope.submitAddEditNoteForm = function(){
        toggleButtonStatus('hide');
        if ($scope.noteName !='' && $scope.noteContent!=''){
            console.log($scope.noteName, $scope.noteContent);
            if ($scope.nonEditedNote.noteName == $scope.noteName && $scope.nonEditedNote.noteContent == $scope.noteContent){
                showErrorMessage($scope,$timeout,'No Changes Made');
                toggleButtonStatus('show');
                return;
            }

            var payload = {
                name:$scope.noteName,
                data:$scope.noteContent,
                id:$stateParams.noteId
            }
            var responsePromise = $http.post('/api/editNote',payload);
            responsePromise.success(function (data, status, headers, config) {
                console.log('response',data,status)
                toggleButtonStatus('show');
                if (status == 200) {
                    showSuccessMessage($scope,$timeout,data.data);
                    $scope.noteName = $scope.noteContent='';
                    $scope.noteEditedSuccessfully = true;
                }
            });
            responsePromise.error(function (data, status, headers, config) {
                console.log('response in error',data,status);
                showErrorMessage($scope,$timeout,data);
                toggleButtonStatus('show');
            });
        }else {
            showErrorMessage($scope,$timeout,"Invalid Form Details");
            toggleButtonStatus('show');
        }

    }

}


