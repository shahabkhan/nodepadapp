angular.module('NodePadApp', ['ui.router']).
    config(['$locationProvider','$stateProvider','$urlRouterProvider', function($locationProvider,$stateProvider,$urlRouterProvider) {
      $locationProvider.html5Mode({requireBase:true, enabled:true});
      $stateProvider
          .state('homePage', {
            url: '/',
            views: {
              "navigation": { templateUrl: "../partials/homePageNavigation.html" },
              "content": { templateUrl: "../partials/homePage.html" }
            }
          })
          .state('logout', {
            url: '/logout',
            views: {
              "navigation": { templateUrl: "../partials/homePageNavigation.html" },
              "content": { templateUrl: "../partials/homePage.html",controller: logoutController
              }
            }

          })
          .state('login', {
            url: '/login',
            views: {
              "navigation": { templateUrl: "../partials/homePageNavigation.html" },
              "content": { templateUrl: "../partials/login.html",controller: loginController }
            }

          })
          .state('register', {
            url: '/register',
            views: {
              "navigation": { templateUrl: "../partials/homePageNavigation.html" },
              "content": { templateUrl: "../partials/register.html",controller: registerController }
            }
          })
          .state('dashboard', {
            url: '/dashboard',
            views: {
              "navigation": { templateUrl: "../partials/dashboard/dashboardNavigation.html" },
              "content": { templateUrl: "../partials/dashboard/index.html",controller: dashboardController }
            }
          })
          .state('addNote', {
            url: '/addNote',
            views: {
              "navigation": { templateUrl: "../partials/dashboard/dashboardNavigation.html" },
              "content": { templateUrl: "../partials/dashboard/addEditNote.html",controller: NoteController }
            }
          })
          .state('editNote', {
            url: '/editNote/:noteId',
            views: {
              "navigation": { templateUrl: "../partials/dashboard/dashboardNavigation.html" },
              "content": { templateUrl: "../partials/dashboard/addEditNote.html",controller: editNoteController }
            }
          })
          .state('viewNotes', {
            url: '/viewNotes',
            views: {
              "navigation": { templateUrl: "../partials/dashboard/dashboardNavigation.html" },
              "content": { templateUrl: "../partials/dashboard/viewNotes.html",controller: NoteController }
            }
          });
      $urlRouterProvider.otherwise("/");
    }]);